const gulp = require('gulp');
const { series } = require("gulp");
const concat = require('gulp-concat');
//Babel makes code compatable with older versions
const babel = require('gulp-babel');
//terser minifies the JS
const terser = require('gulp-terser');
//Handlebars is used for template (*.hbs) processing
const handlebars = require('gulp-handlebars');
const wrap = require('gulp-wrap');
const declare = require('gulp-declare');

//Client 5a: Distribution and performance
const js = () => {
    return gulp.src(['./src/js/*.js'])
        .pipe(babel({
            presets: ['@babel/env']
        }))
        //.pipe(terser())
        .pipe(concat('app.min.js'))
        .pipe(gulp.dest('./dist/'));
}

//export the GULP methods
exports.js = js;

//Client 5b: Templates
const templates = function () {
    return gulp.src('./src/templates/**/*.hbs')
        // Compile each Handlebars template source file to a template function
        .pipe(handlebars())
        // Wrap each template function in a call to Handlebars.template
        .pipe(wrap('Handlebars.template(<%= contents %>)'))
        // Declare template functions as properties and sub-properties of MyApp.templates
        .pipe(declare({
            namespace: 'spa_templates',
            noRedeclare: true, // Avoid duplicate declarations
            processName: function (filePath) {
                // Allow nesting based on path using gulp-declare's processNameByPath()
                // You can remove this option completely if you aren't using nested folders
                // Drop the client/templates/ folder from the namespace path by removing it from the filePath
                return declare.processNameByPath(filePath.replace('<parent_map>/templates/', '')); //windows? backslashes: \\
            }
        }))
        //.pipe(terser())
        .pipe(concat('templates.js'))
        .pipe(gulp.dest('./dist/'));
}

//export the GULP methods
exports.templates = templates;
exports.build = series(templates, js);