/**
 * Say Hello function
 * @param {*} message 
 */
const sayHello = (message) => {
    console.log(message);
    return `Saying ${message}`;
}