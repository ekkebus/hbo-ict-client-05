# hbo-ict-client-05

Run instructions
```
git clone https://gitlab.com/ekkebus/hbo-ict-client-05.git
npm install
```

Installed NPM packages
```
npm install --save-dev gulp gulp-concat gulp-order gulp-terser gulp-babel @babel/core @babel/preset-env

npm install --save-dev handlebars gulp-declare gulp-wrap gulp-handlebars
```
